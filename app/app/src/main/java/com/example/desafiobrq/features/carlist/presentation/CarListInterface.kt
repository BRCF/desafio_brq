package com.example.desafiobrq.features.carlist.presentation

interface CarListInterface {

    interface View {

    }

    interface Presenter {
        fun fetchCars()
    }
}