package com.example.desafiobrq.features.carlist.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.desafiobrq.R
import com.example.desafiobrq.features.carlist.presentation.CarListInterface
import com.example.desafiobrq.features.carlist.presentation.adapter.CarListAdapter
import kotlinx.android.synthetic.main.activity_car_list.*

//RecyclerView.Adapter<RecyclerView.ViewHolder>

class CarListActivity : AppCompatActivity(), CarListInterface.View {

    val adapter: CarListAdapter by lazy {
        CarListAdapter() { ->

        }
    }

//    val presenter: CarListInterface.Presenter =

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_list)
      //  configureRecycleView()
    }

    private fun configureRecycleView() {
        rv_car_list.layoutManager =  LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
        rv_car_list.setHasFixedSize(true)
        rv_car_list.adapter = adapter
    }
}
