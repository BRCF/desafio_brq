# README #

Projeto realizado com base no desafio aplicado pela BRQ para avaliação de novos membros da equipe de Android do projeto Meu TIM.

### ARQUITETURA ###
* MVP

### DEPENDENCIAS ###
* Retrofit2 ~ Camada de serviço
* gson ~ Serialização
* rxjava ~ Comunicação entre as camadas*
* koin ~ Injeção de dependência 
